import bpy
from . import vars
from . import func

class DM_ConvertPencilToMesh(bpy.types.Operator):
    """Convert grease pencil lines to simplified mesh"""
    bl_idname = "gmilligan.dm_convert_pencil_to_mesh"
    bl_label = "Pencil to Mesh"
    bl_description = "Convert the grease pencil lines into mesh lines (Ctl + Shift + D)" 
    bl_options = {'REGISTER'}

    def invoke(self, context, event):

        if self.if_can_convert_pencil_to_mesh(context):

            self.convert_active_pencil_to_mesh(context)
            self.combine_meshes()
            self.end_state(context)
            func.update_mirror(self, context)

        else:

            bpy.ops.gmilligan.dm_edit_mode('INVOKE_DEFAULT')

        return {'FINISHED'}

    def end_state(self, context):
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        meshObj = func.get_first_object_by_name(vars.meshNamePrefix)
        if meshObj:
            meshObj.select_set(True)
        bpy.ops.object.mode_set(mode='EDIT')
        func.set_select_mode(context, 'vertex')
        

    def combine_meshes(self):

        obj_count = 0

        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')

        # select all mesh objects that have the correct name prefix
        for obj in bpy.data.objects:
            if obj.name.startswith(vars.meshNamePrefix):
                if obj.name != vars.meshNamePrefix:
                    obj_count += 1
                    obj.select_set(True)

        if obj_count > 0: 
            # select the main mesh object that will be combined into
            mainMesh = func.get_first_object_by_name(vars.meshNamePrefix)
            mainMesh.select_set(True)

            #join all of the selected meshes
            bpy.ops.object.join()
            # set the edit mode
            bpy.ops.object.mode_set(mode='EDIT')
            # merge vertices that are close together
            bpy.ops.mesh.remove_doubles(threshold=vars.lineThreshold, use_unselected=False) #higher numbers removes more vertices and less precise

    def if_can_convert_pencil_to_mesh(self, context):

        pencilObj = func.get_first_object_by_name(vars.pencilNamePrefix)

        if not pencilObj:
            return False

        if context.mode != 'PAINT_GPENCIL':
            return False

        active_obj = context.view_layer.objects.active

        if active_obj.type != 'GPENCIL':
            return False

        if active_obj.name != vars.pencilNamePrefix:
            return False
        
        #does the grease pencil object have any lines?
        if not active_obj.data.layers.active:
            return False

        if not active_obj.data.layers.active.active_frame:
            return False

        return True

    def convert_active_pencil_to_mesh(self, context):

        override = func.get_context_override_for_type(context)
        if override:

            # first converts to Bezier Curve (must happen in VIEW_3D context which is the purpose of the override)
            bpy.ops.gpencil.convert(override, type='CURVE', use_timing_data=True)
            # clear the grease pencil line(s)
            bpy.ops.gpencil.active_frames_delete_all(override)
            # get the new curve object
            curveObj = func.get_first_object_of_type('CURVE')
            # set the object mode
            bpy.ops.object.mode_set(mode='OBJECT')
            # set the new curve object as the active object (so the mesh conversion is applied to curveObj)
            func.set_object_as_active(context, curveObj)
            # rename the curve object
            context.active_object.name = vars.meshNamePrefix
            # convert curve object into a mesh (finally)
            bpy.ops.object.convert(target='MESH')
            # set the edit mode
            bpy.ops.object.mode_set(mode='EDIT')
            # select the mesh
            bpy.ops.mesh.select_all(action='SELECT')
            # remove excess vertices based on angle
            bpy.ops.mesh.dissolve_limited(angle_limit=vars.lineAngleLimit) #higher numbers removes more vertices
            # merge vertices that are close together
            bpy.ops.mesh.remove_doubles(threshold=vars.lineThreshold, use_unselected=False) #higher numbers removes more vertices and less precise



