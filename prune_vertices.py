import bpy
import bmesh
import math
import mathutils

from . import vars
from . import func

class DM_PruneVertices(bpy.types.Operator):
    """Remove vertices connected to only ONE or ZERO other vertices"""
    bl_idname = "gmilligan.dm_prune_vertices"
    bl_label = "Prune Verts"
    bl_description = "Remove vertices connected to only ONE or ZERO other vertices (Ctl + Shift + P)" 
    bl_options = {'REGISTER'}

    vert_min_connections = 2

    vertices_connected_edges = {}
    vertices_with_min_connections = []

    def invoke(self, context, event):

        meshObj = func.get_first_object_by_name(vars.meshNamePrefix)
        if meshObj:

            func.set_select_mode(context, 'vertex')

            selected_edges = func.get_selected_edges(context, meshObj, False)

            bpy.ops.mesh.select_all(action='DESELECT')
            bpy.ops.object.mode_set(mode='OBJECT')

            self.vertices_connected_edges = func.get_all_vert_connections(selected_edges)
            self.store_verts_with_min_connections()
            self.remove_verts_with_min_connections(context)

        return {'FINISHED'}

    def store_verts_with_min_connections(self):

        self.vertices_with_min_connections = []
        
        for vert_index in self.vertices_connected_edges:
            
            # if this vertex is connected to fewer than self.vert_min_connections other vertices
            if len(self.vertices_connected_edges[vert_index]) < self.vert_min_connections:

                # mark this vertex as one vertex to remove
                self.vertices_with_min_connections.append(vert_index)

    def remove_verts_with_min_connections(self, context):

        if len(self.vertices_with_min_connections) > 0:

            meshObj = func.get_first_object_by_name(vars.meshNamePrefix)

            for vert_index in self.vertices_with_min_connections:

                vert = meshObj.data.vertices[vert_index]
                vert.select = True

        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.delete(type='VERT')
