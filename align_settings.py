import bpy
from bpy.props import BoolProperty
from bpy.props import PointerProperty

def do_set_align_to_pencil(self, context):
    bpy.ops.gmilligan.dm_align_to_pencil('INVOKE_DEFAULT')

class DM_AlignSettings(bpy.types.PropertyGroup):

    axis_lock = bpy.props.EnumProperty(
        name="Axis",
        items=[
            ("horizontal", "Horizontal", '', 1),
            ("vertical", "Vertical", '', 2)
        ],
        description="Lock vertex alignment, to pencil line, on current perspective's straight axis",
        update=do_set_align_to_pencil
    )