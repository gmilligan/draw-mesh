meshNamePrefix = "DM_Mesh"
pencilNamePrefix = "DM_Pencil"

lineAngleLimit = 0.25
lineThreshold = 0.25

maxIntersectDistance = .03
minLineLength = .003

pencilThickness = 12

round_coord_decimals = 4