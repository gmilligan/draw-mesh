import bpy
from . import vars
from . import func

class DM_KeyMapToggleMode(bpy.types.Operator):
    """Key map to handle toggle mode"""
    bl_idname = "gmilligan.dm_keymap_toggle_mode"
    bl_label = "Keymap Toggle Mode"
    bl_description = "Key map to handle toggle mode" 
    bl_options = {'REGISTER'}

    def invoke(self, context, event):
        invoke_def = 'INVOKE_DEFAULT'
        if context.mode == 'PAINT_GPENCIL':
            bpy.ops.gmilligan.dm_edit_mode(invoke_def)
        else:
            bpy.ops.gmilligan.dm_draw_mode(invoke_def)

        return {'FINISHED'}