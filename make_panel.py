import bpy

class DM_MakePanel(bpy.types.Panel):
    """Creates UI"""
    bl_label = "Draw Mesh 3D"
    bl_idname = "gmilligan.dm_make_panel"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"

    def draw(self, context):
        layout = self.layout

        self.create_panel_button(layout, "gmilligan.dm_clear_scene", "X")

        self.create_panel_label(layout, 'MIRRORING', 'MOD_MIRROR')
        mirror_row = layout.row()
        mirror_row.prop(context.scene.dm_mirror_settings, "x", text="X")
        mirror_row.prop(context.scene.dm_mirror_settings, "y", text="Y")
        mirror_row.prop(context.scene.dm_mirror_settings, "z", text="Z")
        mirror_row.prop(context.scene.dm_mirror_settings, "enabled", text="On")

        self.create_panel_label(layout, 'MODE')
        mode_row = layout.row()
        self.create_panel_button(layout, "gmilligan.dm_draw_mode", 'GREASEPENCIL', layout_row=mode_row)
        self.create_panel_button(layout, "gmilligan.dm_edit_mode", 'EDITMODE_HLT', layout_row=mode_row)

        self.create_panel_label(layout, 'CONVERT')
        self.create_panel_button(layout, "gmilligan.dm_convert_pencil_to_mesh", 'MESH_DATA')

        self.create_panel_label(layout, 'ALIGN TO PENCIL')
        align_axis_row = layout.row()
        align_axis_row.prop(context.scene.dm_align_settings, "axis_lock")

        self.create_panel_label(layout, 'MESH')
        mesh_row1 = layout.row()
        self.create_panel_button(mesh_row1, "gmilligan.dm_add_vertex_at_intersection", 'ADD')
        self.create_panel_button(mesh_row1, "gmilligan.dm_prune_vertices", 'REMOVE')
        mesh_row2 = layout.row()
        self.create_panel_button(mesh_row2, "gmilligan.dm_fill_quads", 'FACESEL')
        

        # self.create_panel_label(layout, 'HELPERS')
        # self.create_panel_button(layout, "gmilligan.dm_draw_surface", 'MATPLANE')
        

    def create_panel_button(self, layout, operator, button_icon='', layout_row=False):
        row = layout.row()
        if layout_row:
            row = layout_row
        if len(button_icon) < 1:
            row.operator(operator)
        else:
            row.operator(operator, icon=button_icon)

    def create_panel_label(self, layout, label_text, label_icon=''):
        row = layout.row()
        if len(label_icon) < 1:
            row.label(text=label_text)
        else:
            row.label(text=label_text, icon=label_icon)