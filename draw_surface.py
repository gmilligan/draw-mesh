import bpy

from . import vars
from . import func

class DM_DrawSurface(bpy.types.Operator):
    """Creates a plane, touching 2 or 3 selected points, as a draw surface"""
    bl_idname = "gmilligan.dm_draw_surface"
    bl_label = "Summon Draw Surface"
    bl_description = "Creates a plane, touching 2 or 3 selected points, as a draw surface" 
    bl_options = {'REGISTER'}

    def invoke(self, context, event):
        selected_verts = func.get_selected_verts(context)
        if selected_verts:

            if len(selected_verts) == 2 or len(selected_verts) == 3:

                vert1 = selected_verts[0]
                vert2 = selected_verts[1]
                vert3 = False

                if len(selected_verts) == 3:
                    vert3 = selected_verts[2]

                self.create_draw_surface_touching_verts(context, vert1, vert2, vert3)

        return {'FINISHED'}

    def create_draw_surface_touching_verts(self, context, vert1, vert2, vert3=False):

        override = func.get_context_override_for_type(context)
        if override:

            # put the cursor between the selected 2 or 3 vertices
            bpy.ops.view3d.snap_cursor_to_selected(override)

            print("HERE!")

            # TODO: create plane that touches all 2 to 3 verts

        pass