# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses></http:>.

bl_info = {
    "name" : "Draw Mesh",
    "author" : "gmilligan",
    "description" : "Draw Mesh",
    "blender" : (2, 80, 0),
    "location" : "",
    "warning" : "",
    "category" : "Generic"
}

import bpy
from bpy.props import BoolProperty
from bpy.props import PointerProperty

from . mirror_settings import DM_MirrorSettings
from . align_settings import DM_AlignSettings
from . make_panel import DM_MakePanel
from . clear_scene import DM_ClearScene
from . draw_mode import DM_DrawMode
from . edit_mode import DM_EditMode
from . convert_pencil_to_mesh import DM_ConvertPencilToMesh
from . align_to_pencil import DM_AlignToPencil
from . draw_surface import DM_DrawSurface
from . keymap_toggle_mode import DM_KeyMapToggleMode
from . add_vertex_at_intersection import DM_AddVertexAtIntersection
from . prune_vertices import DM_PruneVertices
from . fill_quads import DM_FillQuads

classes = (
    DM_MakePanel,
    DM_ClearScene,
    DM_ConvertPencilToMesh,
    DM_DrawMode,
    DM_EditMode,
    DM_MirrorSettings,
    DM_AlignSettings,
    DM_AlignToPencil,
    DM_DrawSurface,
    DM_KeyMapToggleMode,
    DM_AddVertexAtIntersection,
    DM_PruneVertices,
    DM_FillQuads,
)

def register_settings():
    bpy.types.Scene.dm_mirror_settings = PointerProperty(type=DM_MirrorSettings)
    bpy.types.Scene.dm_align_settings = PointerProperty(type=DM_AlignSettings)

def unregister_settings():
    del bpy.types.Scene.dm_mirror_settings
    del bpy.types.Scene.dm_align_settings

my_keymaps = []

def add_keymap(my_operator, key, key_action='PRESS', hold_shift=True, hold_ctl=True, view_type=False):
    if not view_type:
        key_configs = bpy.context.window_manager.keyconfigs.addon
        view_type = key_configs.keymaps.new(name='3D View', space_type='VIEW_3D')

    keymap_input = view_type.keymap_items.new(my_operator, key, key_action, shift=hold_shift, ctrl=hold_ctl)
    my_keymaps.append((view_type, keymap_input))

def register_keymaps():
    add_keymap(my_operator='gmilligan.dm_keymap_toggle_mode', key='A')
    add_keymap(my_operator='gmilligan.dm_convert_pencil_to_mesh', key='D')
    add_keymap(my_operator='gmilligan.dm_add_vertex_at_intersection', key='X')
    add_keymap(my_operator='gmilligan.dm_prune_vertices', key='P')
    add_keymap(my_operator='gmilligan.dm_fill_quads', key='Q')

def unregister_keymaps():
    for keymap_3d_view, kmi in my_keymaps:
        keymap_3d_view.keymap_items.remove(kmi)
    my_keymaps.clear()

def register():

    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)

    register_settings()
    register_keymaps()

def unregister(): 

    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)

    unregister_settings()
    unregister_keymaps()

if __name__ == "__main__":
    register()