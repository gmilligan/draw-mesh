import bpy
import math
import bpy_extras
from bpy_extras.view3d_utils import location_3d_to_region_2d
import random

from . import vars
from . import func

class DM_AlignToPencil(bpy.types.Operator):
    """Align the selected vertices to a pencil line"""
    bl_idname = "gmilligan.dm_align_to_pencil"
    bl_label = "Align to Pencil"
    bl_description = "Align the selected vertices to a pencil line, on one 2D axis, from the current perspective of the viewport" 
    bl_options = {'REGISTER'}

    def invoke(self, context, event):
        # must be in edit mode first
        bpy.ops.gmilligan.dm_edit_mode('INVOKE_DEFAULT')

        # if there are any selected verts
        selected_verts = func.get_selected_verts(context)
        if selected_verts:

            if len(selected_verts) > 0:
                
                pencilObj = func.get_first_object_by_name(vars.pencilNamePrefix)

                strokes = func.get_pencil_strokes(pencilObj)
                if strokes:

                    # if there is only one pencil stroke (keep it simple)
                    if len(strokes) == 1:

                        stroke = strokes[0]

                        # must be more than zero points in the stroke
                        if len(stroke.points) > 0:

                            meshObj = func.get_first_object_by_name(vars.meshNamePrefix)

                            self.align_verts_to_pencil_stroke(context, meshObj, stroke)

        return {'FINISHED'}

    # if you use the 2d coordinate of a pencil point, but set the depth of the vertex on it, then you overlap the pencil point while keeping the original vertex depth
    def overlap_2d_perspective_vert_on_point(self, context, region_3d, vert, point):
        # get the pencil stroke point in 2d space, from the current perspective
        sco_2d = func.coord_3d_to_2d(context, region_3d, point.co)

        # calculate the new vertex coordinate that overlaps with the pencil stroke point
        new_vco_3d = func.coord_2d_to_3d(context, region_3d, sco_2d, vert.co)

        # update the vertex
        vert.co = new_vco_3d

    # get the upper and lower axis boundary values based on the peak and vally points of the pencil line
    def get_lowest_and_highest_points_on_straight_axis(self, context, region_3d, stroke, straight_axis):

        lowest_val = 9999999999 # high value to be replaced with first comparison
        highest_val = -1 # low value to be replaced with first comparison

        for point in stroke.points:
            # get the 2D coords for this point
            sco_2d = func.coord_3d_to_2d(context, region_3d, point.co)

            # get the coord value for this pencil point
            this_val = getattr(sco_2d, straight_axis)

            # update lowest value, if needed
            if this_val < lowest_val:
                lowest_val = this_val

            # update highest value, if needed
            if this_val > highest_val:
                highest_val = this_val

        return [lowest_val, highest_val]


    # find the exact point lined up, on a horizontal axis, with this vert
    def get_closest_point_on_straight_axis(self, context, region_3d, stroke, vert, lowest_and_highest_bounds, straight_axis):
        closest_point = False
        smallest_diff = 9999999999 # high value to be replaced with first comparison
        # find the 2D coordinates of this vertex
        vco_2d = func.coord_3d_to_2d(context, region_3d, vert.co)
        # for each point
        for point in stroke.points:
            # get the 2D coords for this point
            sco_2d = func.coord_3d_to_2d(context, region_3d, point.co)
            # get the 2D distance (on the straight axis) from the vertex 2D coordinate
            diff = abs(getattr(sco_2d, straight_axis) - getattr(vco_2d, straight_axis))
            # is this pencil point the closest so far (lined up with vertex on straight_axis)?
            if smallest_diff > diff:
                # if this vertex is not out of bounds to align with a pencil point (pencil line not too short for this vertex)
                if getattr(vco_2d, straight_axis) >= lowest_and_highest_bounds[0] and getattr(vco_2d, straight_axis) <= lowest_and_highest_bounds[1]:
                    # update the closest point so far lined up on a straight axis
                    smallest_diff = diff
                    closest_point = point
                    # if the axis is exact, then the search is over
                    if diff == 0:
                        break
        return closest_point

    def delete_pencil_line(self):
        bpy.ops.gmilligan.dm_draw_mode('INVOKE_DEFAULT')
        bpy.ops.gpencil.active_frames_delete_all()
        bpy.ops.gmilligan.dm_edit_mode('INVOKE_DEFAULT')

    def align_verts_to_pencil_stroke(self, context, meshObj, stroke):

        axis_lock = context.scene.dm_align_settings.axis_lock
        region_3d = func.get_region_3d(context) # needed for: perspective of the current viewport

        # in OBJECT mode, vertices that are moved will actually move
        bpy.ops.object.mode_set(mode='OBJECT')

        # on which axis line should vertices align with a pencil point? 
        which_axis_line = 'x'
        if axis_lock == 'horizontal':
            which_axis_line = 'y'

        # get the upper and lower axis boundary values
        lowest_and_highest_bounds = self.get_lowest_and_highest_points_on_straight_axis(context, region_3d, stroke, which_axis_line)

        # for each vertex
        for vert in meshObj.data.vertices:
            if vert.select:
                # find the exact point lined up, on a horizontal axis, with this vert
                lined_up_point = self.get_closest_point_on_straight_axis(context, region_3d, stroke, vert, lowest_and_highest_bounds, which_axis_line)

                if lined_up_point:
                    # move a vertex so it's overlapping a pencil point, relative to the viewport perspective
                    self.overlap_2d_perspective_vert_on_point(context, region_3d, vert, lined_up_point)

        self.delete_pencil_line()

        # restore edit now that vertices have been updated
        bpy.ops.object.mode_set(mode='EDIT')



        