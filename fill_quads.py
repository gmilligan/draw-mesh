import bpy
import bmesh
import math
import mathutils

from . import vars
from . import func

class DM_FillQuads(bpy.types.Operator):
    """Create faces for quad vertices that don't already have a face"""
    bl_idname = "gmilligan.dm_fill_quads"
    bl_label = "Fill Quads"
    bl_description = "Fill quads for vertices that don't already have a face (Ctl + Shift + Q)" 
    bl_options = {'REGISTER'}

    existing_selected_quad_faces = []

    def invoke(self, context, event):

        meshObj = func.get_first_object_by_name(vars.meshNamePrefix)
        if meshObj:

            self.existing_selected_quad_faces = []
            self.load_existing_selected_quad_faces(meshObj)

            selected_edges = func.get_selected_edges(context, meshObj, False)

            vert_connections = func.get_all_vert_connections(selected_edges)

            quad_vert_indexes = self.get_quad_vert_indexes(vert_connections)

            self.create_quad_faces(context, quad_vert_indexes)

        return {'FINISHED'}

    def load_existing_selected_quad_faces(self, meshObj):

        for face in meshObj.data.polygons:
            if len(face.vertices) == 4:

                quad = []

                for vert in face.vertices:
                    quad.append(vert)

                quad_key = str(sorted(quad))

                if not quad_key in self.existing_selected_quad_faces:
                    self.existing_selected_quad_faces.append(quad_key)

    def get_quad_vert_indexes(self, indexes0):

        quads = []
        quad_keys = []
        
        for vindex0 in indexes0:

            quad = []
            quad.append(vindex0)

            indexes1 = indexes0[vindex0]
            for vindex1 in indexes1:

                if vindex1 in quad:
                    continue
                quad.append(vindex1)

                indexes2 = indexes0[vindex1]
                for vindex2 in indexes2:

                    if vindex2 in quad:
                        continue
                    quad.append(vindex2)

                    indexes3 = indexes0[vindex2]
                    for vindex3 in indexes3:

                        if vindex3 in quad:
                            continue
                        quad.append(vindex3)

                        indexes4 = indexes0[vindex3]
                        for vindex4 in indexes4:

                            if vindex4 == vindex0:

                                new_quad = [vindex0, vindex1, vindex2, vindex3]
                                quad_key = str(sorted(new_quad))
                                if not quad_key in self.existing_selected_quad_faces:
                                    if not quad_key in quad_keys:
                                        quad_keys.append(quad_key)
                                        quads.append(new_quad)

        return quads

    def create_quad_faces(self, context, quad_vert_indexes):

        if len(quad_vert_indexes) > 0:

            func.update_in_edit_mode()
            func.set_select_mode(context, 'vertex')

            meshObj = func.get_first_object_by_name(vars.meshNamePrefix)

            bpy.ops.object.mode_set(mode='EDIT')

            # init bmesh, bm
            bm = bmesh.from_edit_mesh(meshObj.data)

            for quad in quad_vert_indexes:

                quad_verts = []

                for vert_index in quad:

                    vert = meshObj.data.vertices[vert_index]
                    new_vert = bm.verts.new((vert.co.x, vert.co.y, vert.co.z))
                    quad_verts.append(new_vert)

                    vert.select = True
                    new_vert.select = True

                # create the quad face
                bm.faces.new(quad_verts)

            bmesh.update_edit_mesh(meshObj.data)
            bm.free()  # clean up since done using bmesh operations, for now

            # remove doubles
            bpy.ops.mesh.remove_doubles(threshold=0.01, use_unselected=False)
                            






