import bpy

from . import vars
from . import func

class DM_EditMode(bpy.types.Operator):
    """Switch over into edit mode for the mesh"""
    bl_idname = "gmilligan.dm_edit_mode"
    bl_label = "Edit"
    bl_description = "Edit the mesh that was generated from grease pencil (TOGGLE: Ctl + Shift + A)" 
    bl_options = {'REGISTER'}

    def invoke(self, context, event):
        meshObj = func.get_first_object_by_name(vars.meshNamePrefix)
        if meshObj:
            bpy.ops.object.mode_set(mode='OBJECT')
            func.set_object_as_active(context, meshObj)
            bpy.ops.object.mode_set(mode='EDIT')
            func.update_mirror(self, context)
            func.center_cursor(context)
            func.set_select_mode(context, 'vertex')
        return {'FINISHED'}