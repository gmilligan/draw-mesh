import bpy
import bmesh
import math
import mathutils

from . import vars
from . import func

class DM_AddVertexAtIntersection(bpy.types.Operator):
    """Add vertices at intersecting edges"""
    bl_idname = "gmilligan.dm_add_vertex_at_intersection"
    bl_label = "Intersect Verts"
    bl_description = "Add vertices at intersecting edges (Ctl + Shift + X)" 
    bl_options = {'REGISTER'}

    intersecting_edges = {}
    processed_coords = []
    verts_for_edges_to_remove = []

    def invoke(self, context, event):
        meshObj = func.get_first_object_by_name(vars.meshNamePrefix)
        if meshObj:

            self.intersecting_edges = {}
            self.processed_coords = []
            self.verts_for_edges_to_remove = []

            selected_edges = func.get_selected_edges(context, meshObj)

            if len(selected_edges) > 0:

                # set the existing vertices as processed so that they don't get recreated as new intersecting vertices
                self.set_current_vertices_as_processed(meshObj)

                self.find_intersecting_coords(context, meshObj, selected_edges)

                if len(self.intersecting_edges.keys()) > 0:

                    bpy.ops.object.mode_set(mode='EDIT')

                    # init bmesh, bm
                    bm = bmesh.from_edit_mesh(meshObj.data)

                    # create the vertex intersections
                    self.create_intersecting_verts(meshObj, bm)

                    # remove doubles
                    bpy.ops.mesh.remove_doubles(threshold=0.01, use_unselected=False)

                    bm.free()  # clean up since done using bmesh operations, for now

                    # remove the undivided overlapping edges that have been replaced by multiple edges divided by intersections
                    self.remove_extra_edges(context)

        return {'FINISHED'}

    def set_current_vertices_as_processed(self, meshObj):
        for vert in meshObj.data.vertices:
            v = (round(vert.co.x, vars.round_coord_decimals), round(vert.co.y, vars.round_coord_decimals), round(vert.co.z, vars.round_coord_decimals))
            if not v in self.processed_coords:
                self.processed_coords.append(v)

    # return true if the given vertex rests on the given edge
    def is_on_edge(self, v1, edge):

        on_edge = False

        # line points
        p1 = mathutils.Vector((edge['v0'].co.x, edge['v0'].co.y, edge['v0'].co.z))
        p2 = mathutils.Vector((edge['v1'].co.x, edge['v1'].co.y, edge['v1'].co.z))

        line_length = func.get_distance_between_verts(p1, p2)

        # v1 should happen somewhere between p1 and p2
        a = func.get_distance_between_verts(p1, v1)
        b = func.get_distance_between_verts(v1, p2)

        # a + b should be very close to the full line length
        check_sum = line_length - (a + b)

        check_sum = round(check_sum, 2)

        # so checksum should be close to zero
        if check_sum <= vars.maxIntersectDistance and check_sum >= 0:

            on_edge = True

        return on_edge

    def remove_extra_edges(self, context):

        func.set_select_mode(context, 'edge')

        bpy.ops.mesh.select_all(action='DESELECT')

        bpy.ops.object.mode_set(mode='OBJECT')

        meshObj = func.get_first_object_by_name(vars.meshNamePrefix)

        # for each edge, find all the edges that should be deleted based on their two matched vertices
        for edge in meshObj.data.edges:

            v0 = edge.vertices[0]
            v1 = edge.vertices[1]

            vert0 = meshObj.data.vertices[v0]
            vert1 = meshObj.data.vertices[v1]

            v0_round = (round(vert0.co.x, vars.round_coord_decimals), round(vert0.co.y, vars.round_coord_decimals), round(vert0.co.z, vars.round_coord_decimals))
            v1_round = (round(vert1.co.x, vars.round_coord_decimals), round(vert1.co.y, vars.round_coord_decimals), round(vert1.co.z, vars.round_coord_decimals))

            # could this edge be a matched edge that should be removed? 
            edge_key = func.get_edge_key(v0_round, v1_round)
            if edge_key in self.verts_for_edges_to_remove:

                edge.select = True # yes, this was an edge that has been replaced by more than one edge, and therefore, should be removed

        bpy.ops.object.mode_set(mode='EDIT')

        bpy.ops.mesh.delete(type='EDGE')

        func.set_select_mode(context, 'vertex')

    def get_sorted_by_distance_from_point(self, edge_intersections, p0):

        sorted_edge_intersections = []
        distance_order = {}

        v1 = mathutils.Vector((p0.co.x, p0.co.y, p0.co.z))

        # for each intersection point, find the distance to p0, existing point
        for edge_int in edge_intersections:
            v2 = mathutils.Vector((edge_int[0], edge_int[1], edge_int[2]))
            dist = func.get_distance_between_verts(v1, v2)
            distance_order[dist] = edge_int

        # sort the intersection points in order of how far they are from p0, the existing point
        for dist in sorted(distance_order.keys()):
            sorted_edge_intersections.append(distance_order[dist])

        return sorted_edge_intersections

    def create_two_edges_from_one(self, edge_intersections, new_p0, new_p1, bm):
        # create each new vertex
        new_vert = bm.verts.new(edge_intersections[0])
        new_vert.select = True
        bm.edges.new( (new_p0, new_vert) ) # create edge: p0 --> intersection
        bm.edges.new( (new_vert, new_p1) ) # create edge: intersection --> p1

    def create_three_or_more_edges_from_one(self, edge_intersections, new_p0, new_p1, bm):
        # find out the correct SORTED order in which the vertices link up
        edge_intersections = self.get_sorted_by_distance_from_point(edge_intersections, new_p0)

        # for each new vertex (resting on this single edge)
        i = 0
        prev_new_vert = False
        for edge_int in edge_intersections:
            # create each new vertex
            new_vert = bm.verts.new(edge_int)
            new_vert.select = True

            if i == 0:
                bm.edges.new( (new_p0, new_vert) )
            else:
                bm.edges.new( (prev_new_vert, new_vert) )

            # prepare values for next iteration
            prev_new_vert = new_vert
            i+=1

        if prev_new_vert:
            # create the last edge
            bm.edges.new( (prev_new_vert, new_p1) )

    def set_edge_to_remove_in_array(self, v0, v1):
        v0_round = (round(v0[0], vars.round_coord_decimals), round(v0[1], vars.round_coord_decimals), round(v0[2], vars.round_coord_decimals))
        v1_round = (round(v1[0], vars.round_coord_decimals), round(v1[1], vars.round_coord_decimals), round(v1[2], vars.round_coord_decimals))

        edge_key = func.get_edge_key(v0_round, v1_round)
        self.verts_for_edges_to_remove.append(edge_key)

    def create_intersecting_verts(self, meshObj, bm):

        # for each edge that contains one or more intersection
        for edge_id in self.intersecting_edges.keys():
            # get the mesh edge object
            edge = meshObj.data.edges[int(edge_id)]
            # get the first and last vertex of this edge
            p0 = meshObj.data.vertices[edge.vertices[0]]
            p1 = meshObj.data.vertices[edge.vertices[1]]
            v0 = (p0.co.x, p0.co.y, p0.co.z)
            v1 = (p1.co.x, p1.co.y, p1.co.z)
            # track the vertices of the edge that needs to be removed
            self.set_edge_to_remove_in_array(v0, v1)
            # create a new first and last vertex to replace this edge
            new_p0 = bm.verts.new( v0 )
            new_p0.select = True
            new_p1 = bm.verts.new( v1 )
            new_p1.select = True
            # get the array of Vectors where each intersection occurs
            edge_intersections = self.intersecting_edges[edge_id]
            # if there is more than one new vertex added to this single edge
            if len(edge_intersections) > 1:

                self.create_three_or_more_edges_from_one(edge_intersections, new_p0, new_p1, bm)

            else: # only one vertex inserted between p0 and p1 so...

                self.create_two_edges_from_one(edge_intersections, new_p0, new_p1, bm)

    def store_intersection_data(self, meshObj, edge0, edge1):
        intersection_coord = False

        # line 1
        p1 = mathutils.Vector((edge0['v0'].co.x, edge0['v0'].co.y, edge0['v0'].co.z))
        p2 = mathutils.Vector((edge0['v1'].co.x, edge0['v1'].co.y, edge0['v1'].co.z))

        # line 2
        p3 = mathutils.Vector((edge1['v0'].co.x, edge1['v0'].co.y, edge1['v0'].co.z))
        p4 = mathutils.Vector((edge1['v1'].co.x, edge1['v1'].co.y, edge1['v1'].co.z))

        # get the points on both line that are closest to one another
        closest_points = mathutils.geometry.intersect_line_line(p1, p2, p3, p4)

        if closest_points:

            # if this point has not already been found
            rounded_point = (round(closest_points[0][0], vars.round_coord_decimals), round(closest_points[0][1], vars.round_coord_decimals), round(closest_points[0][2], vars.round_coord_decimals))
            if not rounded_point in self.processed_coords:     

                # if the distance between the intersecting points is small enough to be considered an intersection
                distance_between_points = abs(func.get_distance_between_verts(closest_points[0], closest_points[1]))
                if distance_between_points <= vars.maxIntersectDistance:

                    # if the vertex intersection is WITHIN the segment edge lines
                    if self.is_on_edge(closest_points[0], edge0) and self.is_on_edge(closest_points[0], edge1):

                        # set this as an intersection point
                        intersection_coord = closest_points[0]

                        # remember this point as already being found
                        self.processed_coords.append(rounded_point)

                        # remember the intersection data for each edge (what are the coordinates of each intersection for both edges?)
                        if not str(edge0['edge_index']) in self.intersecting_edges:
                            self.intersecting_edges[str(edge0['edge_index'])] = []

                        if not str(edge1['edge_index']) in self.intersecting_edges:
                            self.intersecting_edges[str(edge1['edge_index'])] = []

                        self.intersecting_edges[str(edge0['edge_index'])].append(intersection_coord)
                        self.intersecting_edges[str(edge1['edge_index'])].append(intersection_coord)

        return intersection_coord

    def find_intersecting_coords(self, context, meshObj, selected_edges):
        # for each edge
        for e0, edge0 in enumerate(selected_edges):
            # compare each edge to this edge, except to itself
            for e1, edge1 in enumerate(selected_edges):
                # skips comparing itself, and skips previous comparisons
                if e0 < e1:
                    # if these edges intersect, then get remember the coordinate of intersection
                    self.store_intersection_data(meshObj, edge0, edge1)
