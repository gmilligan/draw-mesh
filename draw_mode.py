import bpy

from . import func

class DM_DrawMode(bpy.types.Operator):
    """Turn on draw mode"""
    bl_idname = "gmilligan.dm_draw_mode"
    bl_label = "Draw"
    bl_description = "Set the mode back to draw mode (TOGGLE: Ctl + Shift + A)" 
    bl_options = {'REGISTER'}

    def invoke(self, context, event):
        func.set_cursor_at_center_of_selected(context)
        self.set_draw_mode(context)
        func.update_mirror(self, context)
        return {'FINISHED'}

    def set_draw_mode(self, context):
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        pencilObj = func.create_pencil(context)

        if pencilObj:
            bpy.ops.object.mode_set(mode='OBJECT')
            func.set_object_as_active(context, pencilObj)
            bpy.ops.gpencil.paintmode_toggle()