import bpy

from . import func

class DM_ClearScene(bpy.types.Operator):
    """Provide functionality to reset a scene"""
    bl_idname = "gmilligan.dm_clear_scene"
    bl_label = "CLEAR ALL!"
    bl_description = "Clearing all objects from scene and setup default objects" 
    bl_options = {'REGISTER'}

    def invoke(self, context, event):
        self.clear_scene()
        func.create_pencil(context)
        func.center_cursor(context)
        func.update_mirror(self, context)
        return {'FINISHED'}

    def clear_scene(self):
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='SELECT')
        bpy.ops.object.delete(use_global=False)
