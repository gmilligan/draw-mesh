import bpy
from bpy.props import BoolProperty
from bpy.props import PointerProperty

from . import func

class DM_MirrorSettings(bpy.types.PropertyGroup):

    enabled = BoolProperty(
        name="Enabled",
        description="Toggle enabling mirroring",
        default = False,
        update=func.update_mirror
        )

    x = BoolProperty(
        name="X-Axis",
        description="Toggle mirroring on the X-Axis",
        default = True,
        update=func.update_mirror
        )

    y = BoolProperty(
        name="Y-Axis",
        description="Toggle mirroring on the Y-Axis",
        default = False,
        update=func.update_mirror
        )

    z = BoolProperty(
        name="Z-Axis",
        description="Toggle mirroring on the Z-Axis",
        default = False,
        update=func.update_mirror
        )