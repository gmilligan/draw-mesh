import bpy
import bpy_extras
import math
import mathutils

from bpy_extras.view3d_utils import location_3d_to_region_2d

from . import vars

def get_first_object_of_type(obj_type):
    found_obj = False

    for obj in bpy.data.objects:
        if obj.type == obj_type:
            found_obj = obj
            break

    return found_obj

def get_first_object_by_name(name):
    found_obj = False

    for obj in bpy.data.objects:
        if obj.name == name:
            found_obj = obj
            break

    return found_obj

def set_select_mode(context, mode='vertex'):
    if mode == 'edge' or mode == 2:
        context.tool_settings.mesh_select_mode = (False, True, False) 
    elif mode == 'face' or mode == 3:
        context.tool_settings.mesh_select_mode = (False, False, True) 
    else:
        context.tool_settings.mesh_select_mode = (True, False, False) 

def create_pencil(context):
    pencilObj = get_first_object_by_name(vars.pencilNamePrefix)
    if not pencilObj:
        bpy.ops.object.gpencil_add(view_align=False, location=(0, 0, 0), type='EMPTY')
        bpy.ops.gpencil.paintmode_toggle()
        # rename
        pencilObj = get_first_object_of_type('GPENCIL')
        pencilObj.name = vars.pencilNamePrefix

        # set stroke thickness
        bpy.data.brushes["Draw Pencil"].size = vars.pencilThickness

    return pencilObj

def set_object_as_active(context, obj):
    context.view_layer.objects.active = obj

def get_distance_between_verts(v1, v2):
    x = v1[0] - v2[0]
    y = v1[1] - v2[1]
    z = v1[2] - v2[2]
    return math.sqrt((x)**2 + (y)**2 + (z)**2) 

def set_cursor(context, x=0, y=0, z=0):
    context.scene.cursor.location[0] = x
    context.scene.cursor.location[1] = y
    context.scene.cursor.location[2] = z

def center_cursor(context):
    set_cursor(context)

def get_pencil_strokes(pencilObj):
    strokes = False
    if pencilObj:
        # any active layers?
        if pencilObj.data.layers.active:
            # any active frames?
            if pencilObj.data.layers.active.active_frame:
                # any strokes
                if pencilObj.data.layers.active.active_frame.strokes:
                    strokes = pencilObj.data.layers.active.active_frame.strokes
    return strokes

def update_in_edit_mode():
    # force an update in edit mode
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.mode_set(mode='EDIT')

def get_selected_verts(context):
    selected_verts = False

    # if in EDIT mode (not in draw mode already)
    if context.mode == 'EDIT_MESH':

        meshObj = get_first_object_by_name(vars.meshNamePrefix)
        if meshObj:
            # make sure the selection is updated and reflects the actual selection status
            update_in_edit_mode()
            #get selected verts
            selected_verts = [v for v in meshObj.data.vertices if v.select]

    return selected_verts

def get_selected_edges(context, meshObj, useMinLength=True):
    update_in_edit_mode()
    set_select_mode(context, 'edge')

    selected_edges = []

    for edge in meshObj.data.edges:
        if edge.select:

            v0 = edge.vertices[0]
            v1 = edge.vertices[1]

            vert0 = meshObj.data.vertices[v0]
            vert1 = meshObj.data.vertices[v1]

            p1 = mathutils.Vector((vert0.co.x, vert0.co.y, vert0.co.z))
            p2 = mathutils.Vector((vert1.co.x, vert1.co.y, vert1.co.z))
            line_length = get_distance_between_verts(p1, p2)

            length_long_enough = line_length >= vars.minLineLength
            if not useMinLength:
                length_long_enough = True

            if length_long_enough:
                selected_edges.append({'edge_index':edge.index, 'v0':vert0, 'v1':vert1, 'v0_index':vert0.index, 'v1_index':vert1.index})

    set_select_mode(context, 'vertex')

    return selected_edges

def get_all_vert_connections(selected_edges):

    vert_connections = {}

    if len(selected_edges) > 0:
        # for each edge
        for edge_data in selected_edges:

            p1_key = edge_data['v0_index']
            p2_key = edge_data['v1_index']

            if not p1_key in vert_connections:
                vert_connections[p1_key] = []

            if not p2_key in vert_connections:
                vert_connections[p2_key] = []

            if not p1_key in vert_connections[p2_key]:
                vert_connections[p2_key].append(p1_key)

            if not p2_key in vert_connections[p1_key]:
                vert_connections[p1_key].append(p2_key)

    return vert_connections

def get_edge_key(v0, v1):
    edge_key = str(v1) + ' | ' + str(v0)
    if v0[0] <= v1[0]:
        edge_key = str(v0) + ' | ' + str(v1)
    return edge_key

def set_cursor_at_center_of_selected(context):
    
    selected_verts = get_selected_verts(context)
    if selected_verts:

        # if only one vertex is selected
        if len(selected_verts) == 1:

            x = selected_verts[0].co.x
            y = selected_verts[0].co.y
            z = selected_verts[0].co.z

            # set the cursor at the only selected vertex
            set_cursor(context, x, y, z)

            # tell blender to draw at the cursor
            context.scene.tool_settings.gpencil_stroke_placement_view3d = 'CURSOR'

        else: # more than one vert selected

            override = get_context_override_for_type(context)
            if override:
                # put the cursor between the selected points
                bpy.ops.view3d.snap_cursor_to_selected(override)

                # tell blender to draw at the cursor's depth
                context.scene.tool_settings.gpencil_stroke_placement_view3d = 'CURSOR'

# convert the 3d coordinates to the equivalent 2d flat coordinates, relative to viewport view
def coord_3d_to_2d(context, region_3d, coord_3d):
    return bpy_extras.view3d_utils.location_3d_to_region_2d(context.region, region_3d, coord_3d)

# convert the 2d coordinates to the equivalent 3d depth coordinates, relative to viewport view
def coord_2d_to_3d(context, region_3d, coord_2d, depth_coord_3d):
    return bpy_extras.view3d_utils.region_2d_to_location_3d(context.region, region_3d, coord_2d, depth_coord_3d)

def update_mirror(self, context):

    # get the current axis settings
    if context.scene.dm_mirror_settings.enabled:
        x = context.scene.dm_mirror_settings.x
        y = context.scene.dm_mirror_settings.y
        z = context.scene.dm_mirror_settings.z
    else:
        x = False
        y = False
        z = False

    # the pencil
    pencilObj = get_first_object_by_name(vars.pencilNamePrefix)
    if pencilObj:
        # if pencil does NOT have mirror yet
        if 'Mirror' not in pencilObj.grease_pencil_modifiers:
            # add mirror modifier to pencil
            pencilObj.grease_pencil_modifiers.new(name="Mirror", type="GP_MIRROR")
            # configure
            pencilObj.grease_pencil_modifiers["Mirror"].show_in_editmode = True
            pencilObj.grease_pencil_modifiers["Mirror"].show_expanded = False

        # set the mirror axis
        pencilObj.grease_pencil_modifiers["Mirror"].x_axis = x
        pencilObj.grease_pencil_modifiers["Mirror"].y_axis = y
        pencilObj.grease_pencil_modifiers["Mirror"].z_axis = z

    # the mesh
    meshObj = get_first_object_by_name(vars.meshNamePrefix)
    if meshObj:
        # if mesh does NOT have mirror yet
        if 'Mirror' not in meshObj.modifiers:
            # add mirror modifier to mesh
            meshObj.modifiers.new(name="Mirror", type="MIRROR")
            # configure
            meshObj.modifiers['Mirror'].show_on_cage = True
            meshObj.modifiers['Mirror'].show_render = True
            meshObj.modifiers['Mirror'].show_viewport = True
            meshObj.modifiers['Mirror'].show_in_editmode = True
            meshObj.modifiers['Mirror'].use_clip = True
            meshObj.modifiers['Mirror'].show_expanded = False

        # set the mirror axis
        meshObj.modifiers['Mirror'].use_axis[0] = x
        meshObj.modifiers['Mirror'].use_axis[1] = y
        meshObj.modifiers['Mirror'].use_axis[2] = z
    

def get_context_override_for_type(context, area_type='VIEW_3D', region_type='WINDOW'):
    override = False
    for window in context.window_manager.windows:
        screen = window.screen

        for area in screen.areas:
            if area.type == area_type:

                for region in area.regions:
                    if region.type == region_type:
                        override = {'window': window, 'scene': context.scene, 'screen': screen, 'area': area, 'region': region}
                        break

                if override:
                    break

    return override

def get_region_3d(context, area_type='VIEW_3D', region_type='WINDOW'):
    region_3d = False
    override = get_context_override_for_type(context, area_type, region_type)
    if override:
        region_3d = override['area'].spaces.active.region_3d
    return region_3d
